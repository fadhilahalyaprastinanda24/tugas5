package id.ac.ub.room;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class InputAdapter extends RecyclerView.Adapter<InputAdapter.InputViewHolder> {
    private List<Item> data;

    public InputAdapter(List<Item> data){
        this.data = data;
    }

    @NonNull
    @Override
    public  InputViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.input_data, parent, false);
        return new InputViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InputViewHolder holder, int position) {
        Item input = data.get(position);
        holder.tvInput.setText(input.getJudul());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class InputViewHolder extends RecyclerView.ViewHolder{
        TextView tvInput;
        public InputViewHolder(View itemView){
            super(itemView);
            tvInput = itemView.findViewById(R.id.tvInputData);
        }
    }
    public void setData(List<Item> newData){
        data.clear();
        data.addAll(newData);
//        notifyDataSetChanged();
    }

}
